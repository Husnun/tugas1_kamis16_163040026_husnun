<?php 

/**
 * 
 */
class Aksesoris_model
{

	private $table = 'aksesoris';
	private $db;
	private $merk;
  	private $jenis;
  	private $bahan;
  	private $harga;
  	private $stok;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function getAllAksesoris()
	{
		$this->db->query("SELECT * FROM " . $this->table);
		return $this->db->resultSet();
	}

	public function getAksesorisById($id)
	{
		$this->db->query("SELECT * FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->single();
	}
	
	public function cariAksesoris($data) {
	    $merk = $data;
	    $query = "SELECT * FROM " . $this->table . " WHERE merk LIKE '%$merk%'";
	    $this->db->query($query);
	    return $this->db->resultSet();
	  }
	
    function hapusAksesoris($id)
	{
   		$this->db->query("DELETE FROM " . $this->table . " WHERE id=:id");
		$this->db->bind("id", $id);
		return $this->db->execute();
		
	}

	public function insertAksesoris($data) {
	    $merk = $data['merk'];
	    $jenis = $data['jenis'];
	    $bahan = $data['bahan'];
	    $harga = $data['harga'];
	    $stok = $data['stok'];

	    $query = "INSERT INTO $this->table VALUES ('', '$merk', '$jenis', '$bahan', '$harga', '$stok')";
	    $this->db->query($query);
	    $this->db->execute();
	}

	public function editAksesoris($data) {
	    $id = $data['id'];
	    $merk = $data['merk'];
	    $jenis = $data['jenis'];
	    $bahan = $data['bahan'];
	    $harga = $data['harga'];
	    $stok = $data['stok'];

	    $query = "UPDATE $this->table SET merk = '$merk', jenis = '$jenis', bahan = '$bahan', harga = '$harga', stok = '$stok' WHERE id = '$id'";
	    $this->db->query($query);
	    $this->db->execute();  
	}
}