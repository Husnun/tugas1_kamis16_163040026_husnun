<?php 


class Aksesoris extends Controller
{
	public function index()
	{
		if (isset($_POST["submit"])) {
			$data = array(
				'judul' => "Daftar Aksesoris",
				'aksesoris' => $this->model('Aksesoris_model')->cariAksesoris($_POST['merk'])
			);
			$this->view('templates/header', $data);
			$this->view('aksesoris/index', $data);
			$this->view('templates/footer');
		} else {
	      $data = array(
	        'judul' => "Daftar Aksesoris",
	        'aksesoris' => $this->model('Aksesoris_model')->getAllAksesoris()
	      );
	      $this->view('templates/header', $data);
	      $this->view('aksesoris/index', $data);
	      $this->view('templates/footer');
	    }	
	}

	public function detail($id)
	{
		$data = array(
			'judul' => "Daftar Aksesoris",
			'aksesoris' => $this->model('Aksesoris_model')->getAksesorisById($id)
		);
		$this->view('templates/header', $data);
		$this->view('aksesoris/detail', $data);
		$this->view('templates/footer');
	}
	
	public function cariAksesoris($data) {
      if (isset($_POST['submit'])) {
        $model = $this->model('Aksesoris_model');
        $data['merk'] = $_POST['merk'];
        $model->cariAksesoris($data);             
      } else {
        $model = $this->model('Aksesoris_model');
        $data = [
          'judul' => "Tambah Aksesoris",
          'aksesoris' => $model->cariAksesoris($data)
        ];
        $this->view('templates/header', $data);
        $this->view('aksesoris/detail', $data);
        $this->view('templates/footer');
      }
    }

	public function hapusAksesoris($id){      
	 	$data = array(
			'judul' => "Daftar Aksesoris Berhasil Di hapus",
			'aksesoris' => $this->model('Aksesoris_model')->hapusAksesoris($id)
		);
		$this->view('templates/header', $data);
		$this->view('aksesoris/hapus', $data);
		$this->view('templates/footer');
    }
    	
	public function insertAksesoris() {
	    if (isset($_POST["submit"])) {
	      $model = $this->model('Aksesoris_model');
	      $data['merk'] = $_POST['merk'];
	      $data['jenis'] = $_POST['jenis'];
	      $data['bahan'] = $_POST['bahan'];
	      $data['harga'] = $_POST['harga'];
	      $data['stok'] = $_POST['stok'];
	      $model->insertAksesoris($data);
	      header("Location: ../Aksesoris");
	    } else {
	        $model = $this->model('Aksesoris_model');
	        $data = [
	          'judul' => "Tambah Aksesoris",
	          'aksesoris' => $model->getAllAksesoris()
	        ];
	        $this->view('templates/header', $data);
	        $this->view('aksesoris/tambah', $data);
	        $this->view('templates/footer');
	    }
    }

    public function editAksesoris($id) {
	    if (isset($_POST["submit"])) {
	      $model = $this->model('Aksesoris_model');
	      $data['id'] = $_POST['id'];
	      $data['merk'] = $_POST['merk'];
	      $data['jenis'] = $_POST['jenis'];
	      $data['bahan'] = $_POST['bahan'];
	      $data['harga'] = $_POST['harga'];
	      $data['stok'] = $_POST['stok'];
	      $model->editAksesoris($data);
	      header("Location: ../Aksesoris");
	    } else {
	        $model = $this->model('Aksesoris_model');
	        $data = [
	          'judul' => "Tambah Aksesoris",
	          'aksesoris' => $model->getAksesorisById($id),
	        ];
	      $this->view('templates/header', $data);
	      $this->view('aksesoris/edit', $data);
	      $this->view('templates/footer');                  
	    }
  	}
}